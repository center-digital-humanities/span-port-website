<?php get_header(); ?>

			<div class="content main" id="main-content">
				<header>
					<h1 class="page-title"><?php the_title(); ?></h1>
					<?php the_content(); ?>
					<div class="filter">
						<div class="fields">
							<h4>Field of Study</h4>
							<ul>
								<button class="button" data-filter=".literature">Literature</button>
								<button class="button" data-filter=".art-film">Art/Film</button>
								<button class="button" data-filter=".language">Language</button>
							</ul>
						</div>
						<div class="language">
							<h4>Language</h4>
							<ul>
								<button class="button" data-filter=".spanish">Spanish</button>
								<button class="button" data-filter=".portuguese">Portuguese</button>
							</ul>
						</div>
					</div>
					<?php wp_nav_menu( array(
						'menu' => 'Faculty Filter',
						'items_wrap' => '<form class="dropdown-filter"><select><option value="*">View All</option>%3$s</select></form>',
						'walker' => new Dropdown_Walker()
					)); ?>
				</header>
				<div class="people-list">
					<ul class="<?php echo $people_cat ?>">
					<?php $core_loop = new WP_Query( array( 'people_cat' => $people_cat, 'post_type' => 'people', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC')); ?>
					<?php while ( $core_loop->have_posts() ) : $core_loop->the_post(); ?>
						<li class="person-item<?php $areas = get_field('area_of_study'); if( $areas ): foreach( $areas as $area ): ?> <?php echo $area->slug; ?><?php endforeach; endif;?><?php $languages = get_field('language_of_study'); if( $languages ): foreach( $languages as $language ): ?> <?php echo $language->slug; ?><?php endforeach; endif;?> <?php if ( $person_link == 'yes' ) { ?>hover<?php } ?>">
							<?php if ( $person_link == 'yes' ) { ?>
							<a href="<?php the_permalink() ?>">
							<?php } ?>
								<?php // if there is a photo, use it
								if(get_field('photo')) {
									$image = get_field('photo');
									if( !empty($image) ): 
										// vars
										$url = $image['url'];
										$title = $image['title'];
										// thumbnail
										$size = 'people-large';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php // otherwise use a silhouette
								} else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php } ?>
								<?php // If this pages uses email and links to pages
								if ( $person_link == 'yes' && $email == 'yes' ) { ?>
								</a>
								<?php } ?>
								<dl>
									<dt class="name">
										<?php // If this pages uses email and links to pages
										if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										<a href="<?php the_permalink() ?>">
										<?php } ?><?php the_title(); ?><?php if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										</a>
										<?php } ?>
									</dt>
									<?php if ( $email == 'yes' ) { 
									if(get_field('email_address')) {
										$person_email = antispambot(get_field('email_address')); ?>
									<dd class="email">
										<a href="mailto:<?php echo $person_email; ?>"><?php echo $person_email; ?></a>
									</dd>
									<?php }
									}
									if ( $phone == 'yes' ) { 
									if(get_field('phone_number')) { ?>
									<dd class="phone"><?php the_field('phone_number'); ?></dd>
									<?php } 
									}
									if ( $position == 'yes' ) {
									if(get_field('position_title')) { ?>
									<dd class="position"><?php the_field('position_title'); ?></dd>
									<?php }
									}
									if ( $interest == 'yes' ) {
									if(get_field('interest')) { ?>
									<dd class="interest"><?php the_field('interest'); ?></dd>
									<?php }
									} ?>
								</dl>
							<?php if ( $person_link == 'yes' ) { ?>
							</a>
							<?php } ?>
						</li>
					<?php endwhile; ?>
					</ul>
				</div>
			</div>
<?php get_footer(); ?>