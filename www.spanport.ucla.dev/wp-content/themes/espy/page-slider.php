<?php
/*
 Template Name: Slide Show
*/
?>
<?php get_header(); ?>

			<?php if(have_rows('hero_image')): ?>
			<script type="text/javascript">
				jQuery("document").ready(function($) {
					$(document).ready(function(){
					  $('#bxslider').bxSlider({
					  	autoHover: true,
					  	auto: false,
					  });
					});
				});
			</script>
			<div id="slider">
				<ul id="bxslider">
					<?php while(have_rows('hero_image')): the_row(); ?>
					<?php
						$slider_title = get_sub_field('title');
						$slider_description = get_sub_field('description');
						$slider_link = get_sub_field('link');
						$silder_image = get_sub_field('image');
						$slider_button = get_sub_field('button_text');
						if(!empty($silder_image)): 
							// vars
							$url = $silder_image['url'];
							$title = $silder_image['title'];
							// thumbnail
							$size = 'home-hero';
							$slide = $silder_image['sizes'][ $size ];
							$width = $silder_image['sizes'][ $size . '-width' ];
							$height = $silder_image['sizes'][ $size . '-height' ];
						endif;
					?>		
					<?php if($slider_link): ?>
					<a href="<?php echo $slider_link; ?>" class="hero-link">
					<?php endif; ?>
						<li style="background-image: url('<?php echo $slide; ?>');">
							<?php if($slider_title || $slider_description): ?>
							<div class="bg">
							<?php endif; ?>
								<div class="content">
									<div class="hero-description <?php the_field('vertical_text_alignment', 'option'); ?>">
										<div class="content <?php the_field('horizontal_text_alignment', 'option'); ?>">
											<?php if($slider_title): ?>
											<h2><?php echo $slider_title; ?></h2>
											<?php endif; ?>
											<?php if($slider_description): ?>
											<p><?php echo $slider_description; ?></p>
											<?php endif; ?>
											<?php if($slider_button): ?>
											<button class="outline"><?php echo $slider_button; ?></button>
											<?php endif; ?>
										</div>
									</div>
								</div>
							<?php if($slider_title || $slider_description): ?>
							</div>
							<?php endif; ?>
							
						</li>
					<?php if($slider_link): ?>
					</a>
					<?php endif; ?>
					<?php endwhile; ?>
				</ul>
			</div>
			<?php endif; ?>

			<div class="content">
				<div class="col" id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1 class="page-title"><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
						</section>
					</article>

				<?php endwhile; else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
						<h1 class="page-title">Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

				<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>