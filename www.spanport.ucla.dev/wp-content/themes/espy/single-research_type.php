<?php
/*
 * RESEARCH POST TYPE TEMPLATE
 *
 * Be aware that you should rename 'custom_cat' and 'custom_tag' to the appropiate custom
 * category and taxonomy slugs, or this template will not finish to load properly.
 *
 * For more info: http://codex.wordpress.org/Post_Type_Templates
*/
?>

<?php get_header(); ?>
			<div class="content">
				<div class="col" id="main-content" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
							
							<h1 class="page-title"><?php the_title(); ?></h1>
							<span class="researcher"><strong>Researcher:</strong> 
							<?php $post_objects = get_field('researcher');
							if( $post_objects ): ?>
							    <?php foreach( $post_objects as $post_object): ?>
							            <a href="<?php echo get_permalink($post_object->ID); ?>"><?php echo get_the_title($post_object->ID); ?></a>
							    <?php endforeach; ?>
							<?php endif; ?>
							
							</span>
							
							<section>
								<?php the_post_thumbnail( 'content-width' ); ?>
								<?php the_content(); ?>
							</section>
							
						</article>

					<?php endwhile; ?>

					<?php else : endif; ?>
					</div>
					<?php get_sidebar(); ?>
				</div>

<?php get_footer(); ?>
