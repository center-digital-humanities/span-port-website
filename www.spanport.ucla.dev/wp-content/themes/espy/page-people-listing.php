<?php
/*
 Template Name: People Listing
*/
?>
<?php get_header(); ?>
			<div class="content main full-width" id="main-content">
				<header>
					<h1 class="page-title"><?php the_title(); ?></h1>
					<?php the_content(); ?>
				
					<?php // Select what people category to show
					$people_category = get_field('people_category');
					if( $people_category ) {
						$people_cat = $people_category->slug;
					}
					// Set varaibles to decide behavior of page
					if ( get_field('link_to_pages') == 'yes' ) {
						$person_link = 'yes';
					}
                    if (get_field('office_hours_btn') == 'show' ) {
                        $office_hours_btn = 'yes';
                    }                    
                    if ( get_field('bottom_content') ) {
						$bottom_content = get_field('bottom_content');
					}
					$people_details = get_field('people_details');
					if( in_array('position', $people_details) ) { 
						$position = 'yes';
					}
					if( in_array('interest', $people_details) ) {
						$interest = 'yes';
					}
					if( in_array('email', $people_details) ) {
						$email = 'yes';
					}
					if( in_array('phone', $people_details) ) {
						$phone = 'yes';
					}
					if( in_array('office', $people_details) ) {
						$office = 'yes';
					}
					?>
					<?php if ( get_field('display_field_of_study') == 'show' ) { ?>
					<div class="filter">
					<?php // To make another filter, duplicate the div below ?>
						<div class="options button-group" data-filter-group="field">
							<h5>Field of Study</h5>
							<ul>
								<button data-filter="" data-text="All" class="option btn all is-checked">All</button>
								<button class="option btn literature" data-filter=".literature">Literature/Culture</button>
								<button class="option btn" data-filter=".linguistics">Linguistics</button>
							</ul>
						</div>
						<div class="language button-group" data-filter-group="language">
							<h5>Language</h5>
							<ul>
								<button class="option btn all is-checked" data-filter="">All</button>
								<button class="option btn" data-filter=".Spanish">Spanish</button>
								<button class="option btn" data-filter=".Portuguese">Portuguese</button>
							</ul>
						</div>
					</div>
					<?php 
					/*
						wp_nav_menu( array(
							'menu' => 'Faculty Filter',
							'items_wrap' => '<form class="dropdown-filter"><select><option value="*">View All</option>%3$s</select></form>',
							'walker' => new Dropdown_Walker()
						));
					*/
				} ?>
				</header>
				<div class="people-list">
					<ul class="<?php echo $people_cat ?>">
					<?php 
						$core_loop = new WP_Query( array( 'people_cat' => $people_cat, 'post_type' => 'people_type', 'posts_per_page' => -1,  'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC', 'meta_query' => array( array( 'key' => 'person_type', 'value'   => 'core'))));
						
						$lectures_loop = new WP_Query( array( 'people_cat' => $people_cat, 'post_type' => 'people_type', 'posts_per_page' => -1,  'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC', 'meta_query' => array( array( 'key' => 'person_type', 'value'   => 'lectures'))));
						
						$continuing_loop = new WP_Query( array( 'people_cat' => $people_cat, 'post_type' => 'people_type', 'posts_per_page' => -1,  'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC', 'meta_query' => array( array( 'key' => 'person_type', 'value'   => 'continuing'))));
						
						$fellows_loop = new WP_Query( array( 'people_cat' => $people_cat, 'post_type' => 'people_type', 'posts_per_page' => -1,  'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC', 'meta_query' => array( array( 'key' => 'person_type', 'value'   => 'fellows'))));
						
						$visiting_loop = new WP_Query( array( 'people_cat' => $people_cat, 'post_type' => 'people_type', 'posts_per_page' => -1,  'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC', 'meta_query' => array( array( 'key' => 'person_type', 'value'   => 'visiting'))));
						
						$emeriti_loop = new WP_Query( array( 'people_cat' => $people_cat, 'post_type' => 'people_type', 'posts_per_page' => -1,  'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC', 'meta_query' => array( array( 'key' => 'person_type', 'value'   => 'emeriti'))));
						
						?>
					<?php while ( $core_loop->have_posts() ) : $core_loop->the_post(); ?>
						<li class="person-item <?php the_field('language_of_study'); ?> <?php echo implode(' ', get_field('field')); ?> <?php if ( $person_link == 'yes' ) { ?>hover<?php } ?>">
							<?php if ( $person_link == 'yes' ) { ?>
							<a href="<?php the_permalink() ?>">
							<?php } ?>
								<?php // if there is a photo, use it
								if(get_field('photo')) {
									$image = get_field('photo');
									if( !empty($image) ): 
										// vars
										$url = $image['url'];
										$title = $image['title'];
										// thumbnail
										$size = 'people-large';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
									endif; ?>
									<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
									<?php // otherwise use a silhouette
									} else { ?>
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
									<?php } ?>
								<?php // If this pages uses email and links to pages
								if ( $person_link == 'yes' && $email == 'yes' ) { ?>
								</a>
								<?php } ?>
								<dl>
									<dt class="name">
										<?php // If this pages uses email and links to pages
										if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										<a href="<?php the_permalink() ?>">
										<?php } ?><?php the_title(); ?><?php if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										</a>
										<?php } ?>
									</dt>
									<?php
									if ( $position == 'yes' ) {
										if(get_field('position_title')) { ?>
										<dd class="position"><?php the_field('position_title'); ?></dd>
										<?php }
									}
									if ( $phone == 'yes' ) {
										if(get_field('phone_number')) { ?>
										<dd class="phone"><?php the_field('phone_number'); ?></dd>
										<?php } 
									}
									if ( $office == 'yes' ) {
										if(get_field('office')) { ?>
										<dd class="office"><?php the_field('office'); ?></dd>
										<?php } 
									}
									if ( $email == 'yes' ) {
										if(get_field('email_address')) {
											$person_email = antispambot(get_field('email_address')); ?>
										<dd class="email">
											<a href="mailto:<?php echo $person_email; ?>"><?php echo $person_email; ?></a>
										</dd>
										<?php }
									}
									if ( $interest == 'yes' ) {
										if(get_field('interest')) { ?>
										<dd class="interest"><?php the_field('interest'); ?></dd>
										<?php }
									}
									if ( $office_hours_btn == 'yes' ) {
										if(get_field('office_hours')) { ?>
										<dd class="office"><strong>Work Schedule: </strong><?php the_field('office_hours'); ?></dd>
										<?php }
									} 
									?>
								</dl>
							<?php if ( $person_link == 'yes' ) { ?>
							</a>
							<?php } ?>
						</li>
					<?php endwhile; ?>
					
					<?php while ( $lectures_loop->have_posts() ) : $lectures_loop->the_post(); ?>
						<li class="person-item <?php the_field('language_of_study'); ?> <?php echo implode(' ', get_field('field')); ?> <?php if ( $person_link == 'yes' ) { ?>hover<?php } ?>">
							<?php if ( $person_link == 'yes' ) { ?>
							<a href="<?php the_permalink() ?>">
							<?php } ?>
								<?php // if there is a photo, use it
								if(get_field('photo')) {
									$image = get_field('photo');
									if( !empty($image) ): 
										// vars
										$url = $image['url'];
										$title = $image['title'];
										// thumbnail
										$size = 'people-large';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
									endif; ?>
									<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
									<?php // otherwise use a silhouette
									} else { ?>
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
									<?php } ?>
								<?php // If this pages uses email and links to pages
								if ( $person_link == 'yes' && $email == 'yes' ) { ?>
								</a>
								<?php } ?>
								<dl>
									<dt class="name">
										<?php // If this pages uses email and links to pages
										if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										<a href="<?php the_permalink() ?>">
										<?php } ?><?php the_title(); ?><?php if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										</a>
										<?php } ?>
									</dt>
									<?php
									if ( $position == 'yes' ) {
										if(get_field('position_title')) { ?>
										<dd class="position"><?php the_field('position_title'); ?></dd>
										<?php }
									}
									if ( $phone == 'yes' ) {
										if(get_field('phone_number')) { ?>
										<dd class="phone"><?php the_field('phone_number'); ?></dd>
										<?php } 
									}
									if ( $office == 'yes' ) {
										if(get_field('office')) { ?>
										<dd class="office"><?php the_field('office'); ?></dd>
										<?php } 
									}
									if ( $email == 'yes' ) {
										if(get_field('email_address')) {
											$person_email = antispambot(get_field('email_address')); ?>
										<dd class="email">
											<a href="mailto:<?php echo $person_email; ?>"><?php echo $person_email; ?></a>
										</dd>
										<?php }
									}
									if ( $interest == 'yes' ) {
										if(get_field('interest')) { ?>
										<dd class="interest"><?php the_field('interest'); ?></dd>
										<?php }
									}
									if ( $office_hours_btn == 'yes' ) {
										if(get_field('office_hours')) { ?>
										<dd class="hours"><strong>Work Schedule: </strong><?php the_field('office_hours'); ?></dd>
										<?php }
									} 
									?>
								</dl>
							<?php if ( $person_link == 'yes' ) { ?>
							</a>
							<?php } ?>
						</li>
					<?php endwhile; ?>
					
					<?php while ( $continuing_loop->have_posts() ) : $continuing_loop->the_post(); ?>
						<li class="person-item <?php the_field('language_of_study'); ?> <?php echo implode(' ', get_field('field')); ?> <?php if ( $person_link == 'yes' ) { ?>hover<?php } ?>">
							<?php if ( $person_link == 'yes' ) { ?>
							<a href="<?php the_permalink() ?>">
							<?php } ?>
								<?php // if there is a photo, use it
								if(get_field('photo')) {
									$image = get_field('photo');
									if( !empty($image) ): 
										// vars
										$url = $image['url'];
										$title = $image['title'];
										// thumbnail
										$size = 'people-large';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
									endif; ?>
									<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
									<?php // otherwise use a silhouette
									} else { ?>
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
									<?php } ?>
								<?php // If this pages uses email and links to pages
								if ( $person_link == 'yes' && $email == 'yes' ) { ?>
								</a>
								<?php } ?>
								<dl>
									<dt class="name">
										<?php // If this pages uses email and links to pages
										if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										<a href="<?php the_permalink() ?>">
										<?php } ?><?php the_title(); ?><?php if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										</a>
										<?php } ?>
									</dt>
									<?php
									if ( $position == 'yes' ) {
										if(get_field('position_title')) { ?>
										<dd class="position"><?php the_field('position_title'); ?></dd>
										<?php }
									}
									if ( $phone == 'yes' ) {
										if(get_field('phone_number')) { ?>
										<dd class="phone"><?php the_field('phone_number'); ?></dd>
										<?php } 
									}
									if ( $office == 'yes' ) {
										if(get_field('office')) { ?>
										<dd class="office"><?php the_field('office'); ?></dd>
										<?php } 
									}
									if ( $email == 'yes' ) {
										if(get_field('email_address')) {
											$person_email = antispambot(get_field('email_address')); ?>
										<dd class="email">
											<a href="mailto:<?php echo $person_email; ?>"><?php echo $person_email; ?></a>
										</dd>
										<?php }
									}
									if ( $interest == 'yes' ) {
										if(get_field('interest')) { ?>
										<dd class="interest"><?php the_field('interest'); ?></dd>
										<?php }
									} 
									if ( $office_hours_btn == 'yes' ) {
										if(get_field('office_hours')) { ?>
										<dd class="office"><strong>Work Schedule: </strong><?php the_field('office_hours'); ?></dd>
										<?php }
									} 
									?>
								</dl>
							<?php if ( $person_link == 'yes' ) { ?>
							</a>
							<?php } ?>
						</li>
					<?php endwhile; ?>
					
					<?php while ( $fellows_loop->have_posts() ) : $fellows_loop->the_post(); ?>
						<li class="person-item <?php the_field('language_of_study'); ?> <?php echo implode(' ', get_field('field')); ?> <?php if ( $person_link == 'yes' ) { ?>hover<?php } ?>">
							<?php if ( $person_link == 'yes' ) { ?>
							<a href="<?php the_permalink() ?>">
							<?php } ?>
								<?php // if there is a photo, use it
								if(get_field('photo')) {
									$image = get_field('photo');
									if( !empty($image) ): 
										// vars
										$url = $image['url'];
										$title = $image['title'];
										// thumbnail
										$size = 'people-large';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
									endif; ?>
									<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
									<?php // otherwise use a silhouette
									} else { ?>
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
									<?php } ?>
								<?php // If this pages uses email and links to pages
								if ( $person_link == 'yes' && $email == 'yes' ) { ?>
								</a>
								<?php } ?>
								<dl>
									<dt class="name">
										<?php // If this pages uses email and links to pages
										if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										<a href="<?php the_permalink() ?>">
										<?php } ?><?php the_title(); ?><?php if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										</a>
										<?php } ?>
									</dt>
									<?php
									if ( $position == 'yes' ) {
										if(get_field('position_title')) { ?>
										<dd class="position"><?php the_field('position_title'); ?></dd>
										<?php }
									}
									if ( $phone == 'yes' ) {
										if(get_field('phone_number')) { ?>
										<dd class="phone"><?php the_field('phone_number'); ?></dd>
										<?php } 
									}
									if ( $office == 'yes' ) {
										if(get_field('office')) { ?>
										<dd class="office"><?php the_field('office'); ?></dd>
										<?php } 
									}
									if ( $email == 'yes' ) {
										if(get_field('email_address')) {
											$person_email = antispambot(get_field('email_address')); ?>
										<dd class="email">
											<a href="mailto:<?php echo $person_email; ?>"><?php echo $person_email; ?></a>
										</dd>
										<?php }
									}
									if ( $interest == 'yes' ) {
										if(get_field('interest')) { ?>
										<dd class="interest"><?php the_field('interest'); ?></dd>
										<?php }
									} 
									if ( $office_hours_btn == 'yes' ) {
										if(get_field('office_hours')) { ?>
										<dd class="office"><strong>Work Schedule: </strong><?php the_field('office_hours'); ?></dd>
										<?php }
									} 
									?>
								</dl>
							<?php if ( $person_link == 'yes' ) { ?>
							</a>
							<?php } ?>
						</li>
					<?php endwhile; ?>
					
					<?php while ( $visiting_loop->have_posts() ) : $visiting_loop->the_post(); ?>
						<li class="person-item <?php the_field('language_of_study'); ?> <?php echo implode(' ', get_field('field')); ?> <?php if ( $person_link == 'yes' ) { ?>hover<?php } ?>">
							<?php if ( $person_link == 'yes' ) { ?>
							<a href="<?php the_permalink() ?>">
							<?php } ?>
								<?php // if there is a photo, use it
								if(get_field('photo')) {
									$image = get_field('photo');
									if( !empty($image) ): 
										// vars
										$url = $image['url'];
										$title = $image['title'];
										// thumbnail
										$size = 'people-large';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
									endif; ?>
									<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
									<?php // otherwise use a silhouette
									} else { ?>
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
									<?php } ?>
								<?php // If this pages uses email and links to pages
								if ( $person_link == 'yes' && $email == 'yes' ) { ?>
								</a>
								<?php } ?>
								<dl>
									<dt class="name">
										<?php // If this pages uses email and links to pages
										if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										<a href="<?php the_permalink() ?>">
										<?php } ?><?php the_title(); ?><?php if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										</a>
										<?php } ?>
									</dt>
									<?php
									if ( $position == 'yes' ) {
										if(get_field('position_title')) { ?>
										<dd class="position"><?php the_field('position_title'); ?></dd>
										<?php }
									}
									if ( $phone == 'yes' ) {
										if(get_field('phone_number')) { ?>
										<dd class="phone"><?php the_field('phone_number'); ?></dd>
										<?php } 
									}
									if ( $office == 'yes' ) {
										if(get_field('office')) { ?>
										<dd class="office"><?php the_field('office'); ?></dd>
										<?php } 
									}
									if ( $email == 'yes' ) {
										if(get_field('email_address')) {
											$person_email = antispambot(get_field('email_address')); ?>
										<dd class="email">
											<a href="mailto:<?php echo $person_email; ?>"><?php echo $person_email; ?></a>
										</dd>
										<?php }
									}
									if ( $interest == 'yes' ) {
										if(get_field('interest')) { ?>
										<dd class="interest"><?php the_field('interest'); ?></dd>
										<?php }
									} 
									if ( $office_hours_btn == 'yes' ) {
										if(get_field('office_hours')) { ?>
										<dd class="office"><strong>Work Schedule: </strong><?php the_field('office_hours'); ?></dd>
										<?php }
									} 
									?>
								</dl>
							<?php if ( $person_link == 'yes' ) { ?>
							</a>
							<?php } ?>
						</li>
					<?php endwhile; ?>
					
					<?php while ( $emeriti_loop->have_posts() ) : $emeriti_loop->the_post(); ?>
						<li class="person-item <?php the_field('language_of_study'); ?> <?php echo implode(' ', get_field('field')); ?> <?php if ( $person_link == 'yes' ) { ?>hover<?php } ?>">
							<?php if ( $person_link == 'yes' ) { ?>
							<a href="<?php the_permalink() ?>">
							<?php } ?>
								<?php // if there is a photo, use it
								if(get_field('photo')) {
									$image = get_field('photo');
									if( !empty($image) ): 
										// vars
										$url = $image['url'];
										$title = $image['title'];
										// thumbnail
										$size = 'people-large';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
									endif; ?>
									<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
									<?php // otherwise use a silhouette
									} else { ?>
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
									<?php } ?>
								<?php // If this pages uses email and links to pages
								if ( $person_link == 'yes' && $email == 'yes' ) { ?>
								</a>
								<?php } ?>
								<dl>
									<dt class="name">
										<?php // If this pages uses email and links to pages
										if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										<a href="<?php the_permalink() ?>">
										<?php } ?><?php the_title(); ?><?php if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										</a>
										<?php } ?>
									</dt>
									<?php
									if ( $position == 'yes' ) {
										if(get_field('position_title')) { ?>
										<dd class="position"><?php the_field('position_title'); ?></dd>
										<?php }
									}
									if ( $phone == 'yes' ) {
										if(get_field('phone_number')) { ?>
										<dd class="phone"><?php the_field('phone_number'); ?></dd>
										<?php } 
									}
									if ( $office == 'yes' ) {
										if(get_field('office')) { ?>
										<dd class="office"><?php the_field('office'); ?></dd>
										<?php } 
									}
									if ( $email == 'yes' ) {
										if(get_field('email_address')) {
											$person_email = antispambot(get_field('email_address')); ?>
										<dd class="email">
											<a href="mailto:<?php echo $person_email; ?>"><?php echo $person_email; ?></a>
										</dd>
										<?php }
									}
									if ( $interest == 'yes' ) {
										if(get_field('interest')) { ?>
										<dd class="interest"><?php the_field('interest'); ?></dd>
										<?php }
									} 
									if ( $office_hours_btn == 'yes' ) {
										if(get_field('office_hours')) { ?>
										<dd class="office"><strong>Work Schedule: </strong><?php the_field('office_hours'); ?></dd>
										<?php }
									} 
									?>
								</dl>
							<?php if ( $person_link == 'yes' ) { ?>
							</a>
							<?php } ?>
						</li>
					<?php endwhile; ?>
					</ul>
				</div>
				<?php if (!empty($bottom_content)) {?>
					<div class="additional-content">
						<?php echo $bottom_content; ?>
					</div>
				<?php } ?>
			</div>
			
<?php get_footer(); ?>