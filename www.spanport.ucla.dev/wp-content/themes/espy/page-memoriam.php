<?php
/*
 Template Name: In Memoriam
*/
?>
<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1 class="page-title"><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
						</section>
						<section>
						<?php if( have_rows('people') ): ?>
						
							<ul class="in-memoriam">
							<?php while( have_rows('people') ): the_row(); 
						
								// vars
								$memoriam_name = get_sub_field('name');
								$memoriam_years = get_sub_field('years');
								$memoriam_bio = get_sub_field('bio');
								?>
						
								<li class="person">
									<?php // if there is a photo, use it
									if(get_sub_field('photo')) {
										$image = get_sub_field('photo');
										if( !empty($image) ): 
											// vars
											$url = $image['url'];
											$title = $image['title'];
											// thumbnail
											$size = 'bones-thumb-100';
											$thumb = $image['sizes'][ $size ];
											$width = $image['sizes'][ $size . '-width' ];
											$height = $image['sizes'][ $size . '-height' ];
										endif; ?>
										<img src="<?php echo $thumb; ?>" alt="A photo of <?php echo $memoriam_name; ?>" class="photo"/>
										<?php // otherwise use a silhouette
										} else { ?>
										<img src="<?php echo get_template_directory_uri(); ?>/library/images/placeholder-ucla.jpg" alt="A placeholder photo for <?php echo $memoriam_name; ?>" class="photo"/>
									<?php } ?>
									<dl>
										<?php if( $memoriam_name ): ?>
											<dt class="name"><h3><?php echo $memoriam_name; ?></h3></dt>
										<?php endif; ?>
									    <?php if( $memoriam_years ): ?>
									    	<dd class="year"><?php echo $memoriam_years; ?></dd>
									    <?php endif; ?>
									    <?php if( $memoriam_bio ): ?>
									    	<dd class="bio"><?php echo $memoriam_bio; ?></dd>
									    <?php endif; ?>
								    </dl>
								</li>
						
							<?php endwhile; ?>
						
							</ul>
						
						<?php endif; ?>
						</section>

					</article>

					<?php endwhile; else : ?>

					<article id="post-not-found" class="hentry cf">
							<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
						<section class="entry-content">
							<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
						</section>
								<p><?php _e( 'This is the error message in the page.php template.', 'bonestheme' ); ?></p>
					</article>

					<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>