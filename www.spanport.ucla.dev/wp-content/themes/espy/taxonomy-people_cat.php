<?php get_header(); ?>

			<div class="content main">
			
				<header>
					<h1>Faculty</h1>
					<p>Our faculty consist of over 20 of the most talented minds in the field.</p>
					<!-- <input type="text" class="search-filter" placeholder="Type a name..." /> -->
					<div class="filter">
						<div class="fields button-group" data-filter-group="field">
							<h4>Field of Study</h4>
							<ul>
								<button class="button btn all is-checked" data-filter="">All</button>
								<button class="button btn literature" data-filter=".literature">Literature/Culture</button>
								<button class="button btn" data-filter=".linguistics">Linguistics</button>
							</ul>
						</div>
						<div class="language button-group" data-filter-group="language">
							<h4>Language</h4>
							<ul>
								<button class="button btn all is-checked" data-filter="">All</button>
								<button class="button btn" data-filter=".Spanish">Spanish</button>
								<button class="button btn" data-filter=".Portuguese">Portuguese</button>
							</ul>
						</div>
					</div>
				</header>

				<div class="people-list">

					<ul <?php post_class('cf'); ?>>
					
					<?php
											
						$core_loop = new WP_Query( array( 'people_cat' => 'faculty', 'post_type' => 'people_type', 'posts_per_page' => -1,  'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC', 'meta_query' => array( array( 'key' => 'person_type', 'value'   => 'core'))));
						
						$lectures_loop = new WP_Query( array( 'people_cat' => 'faculty', 'post_type' => 'people_type', 'posts_per_page' => -1,  'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC', 'meta_query' => array( array( 'key' => 'person_type', 'value'   => 'lectures'))));
						
						$continuing_loop = new WP_Query( array( 'people_cat' => 'faculty', 'post_type' => 'people_type', 'posts_per_page' => -1,  'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC', 'meta_query' => array( array( 'key' => 'person_type', 'value'   => 'continuing'))));
						
						$fellows_loop = new WP_Query( array( 'people_cat' => 'faculty', 'post_type' => 'people_type', 'posts_per_page' => -1,  'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC', 'meta_query' => array( array( 'key' => 'person_type', 'value'   => 'fellows'))));
						
						$visiting_loop = new WP_Query( array( 'people_cat' => 'faculty', 'post_type' => 'people_type', 'posts_per_page' => -1,  'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC', 'meta_query' => array( array( 'key' => 'person_type', 'value'   => 'visiting'))));
						
						$emeriti_loop = new WP_Query( array( 'people_cat' => 'faculty', 'post_type' => 'people_type', 'posts_per_page' => -1,  'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC', 'meta_query' => array( array( 'key' => 'person_type', 'value'   => 'emeriti'))));
						
					?>
					
					<?php while ( $core_loop->have_posts() ) : $core_loop->the_post(); ?>
										
						<a href="<?php the_permalink() ?>" class="person-item <?php the_field('language_of_study'); ?> <?php echo implode(' ', get_field('field')); ?>">
							<li><?php if(get_field('photo')) {
								$image = get_field('photo');
								if( !empty($image) ): 
									// vars
									$url = $image['url'];
									$title = $image['title'];
									// thumbnail
									$size = 'bones-thumb-100';
									$thumb = $image['sizes'][ $size ];
									$width = $image['sizes'][ $size . '-width' ];
									$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="photo" />
								<?php } else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" width="100px" height="100px" class="photo" />
								<?php } ?>
								<dl>
									<dt class="name"><?php the_title(); ?></dt>
									<dd class="position"><?php the_field('position_title'); ?></dd>
									<?php if(get_field('interest')) { ?>
									<dd class="interest">
										<!--<strong>Interest</strong>-->
										<?php the_field('interest'); ?>
									</dd>
									<?php } ?>
								</dl>
							</li>
						</a>

					<?php endwhile; ?>
					
					<?php while ( $visiting_loop->have_posts() ) : $visiting_loop->the_post(); ?>
															
						<a href="<?php the_permalink() ?>" class="person-item <?php the_field('language_of_study'); ?> <?php echo implode(' ', get_field('field')); ?>">
							<li><?php if(get_field('photo')) {
								$image = get_field('photo');
								if( !empty($image) ): 
									// vars
									$url = $image['url'];
									$title = $image['title'];
									// thumbnail
									$size = 'bones-thumb-100';
									$thumb = $image['sizes'][ $size ];
									$width = $image['sizes'][ $size . '-width' ];
									$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="photo" />
								<?php } else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" width="100px" height="100px" class="photo" />
								<?php } ?>
								<dl>
									<dt class="name"><?php the_title(); ?></dt>
									<dd class="position"><?php the_field('position_title'); ?></dd>
									<?php if(get_field('interest')) { ?>
									<dd class="interest">
										<!--<strong>Interest</strong>-->
										<?php the_field('interest'); ?>
									</dd>
									<?php } ?>
								</dl>
							</li>
						</a>

					<?php endwhile; ?>
					
					<?php while ( $continuing_loop->have_posts() ) : $continuing_loop->the_post(); ?>
															
						<a href="<?php the_permalink() ?>" class="person-item <?php the_field('language_of_study'); ?> <?php echo implode(' ', get_field('field')); ?>">
							<li><?php if(get_field('photo')) {
								$image = get_field('photo');
								if( !empty($image) ): 
									// vars
									$url = $image['url'];
									$title = $image['title'];
									// thumbnail
									$size = 'bones-thumb-100';
									$thumb = $image['sizes'][ $size ];
									$width = $image['sizes'][ $size . '-width' ];
									$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="photo" />
								<?php } else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" width="100px" height="100px" class="photo" />
								<?php } ?>
								<dl>
									<dt class="name"><?php the_title(); ?></dt>
									<dd class="position"><?php the_field('position_title'); ?></dd>
									<?php if(get_field('interest')) { ?>
									<dd class="interest">
										<!--<strong>Interest</strong>-->
										<?php the_field('interest'); ?>
									</dd>
									<?php } ?>
								</dl>
							</li>
						</a>

					<?php endwhile; ?>

					<?php while ( $lectures_loop->have_posts() ) : $lectures_loop->the_post(); ?>
										
						<a href="<?php the_permalink() ?>" class="person-item <?php the_field('language_of_study'); ?> <?php echo implode(' ', get_field('field')); ?>">
							<li><?php if(get_field('photo')) {
								$image = get_field('photo');
								if( !empty($image) ): 
									// vars
									$url = $image['url'];
									$title = $image['title'];
									// thumbnail
									$size = 'bones-thumb-100';
									$thumb = $image['sizes'][ $size ];
									$width = $image['sizes'][ $size . '-width' ];
									$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="photo" />
								<?php } else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" width="100px" height="100px" class="photo" />
								<?php } ?>
								<dl>
									<dt class="name"><?php the_title(); ?></dt>
									<dd class="position"><?php the_field('position_title'); ?></dd>
									<?php if(get_field('interest')) { ?>
									<dd class="interest">
										<!--<strong>Interest</strong>-->
										<?php the_field('interest'); ?>
									</dd>
									<?php } ?>
								</dl>
							</li>
						</a>

					<?php endwhile; ?>
					
					<?php while ( $fellows_loop->have_posts() ) : $fellows_loop->the_post(); ?>
										
						<a href="<?php the_permalink() ?>" class="person-item <?php the_field('language_of_study'); ?> <?php echo implode(' ', get_field('field')); ?>">
							<li><?php if(get_field('photo')) {
								$image = get_field('photo');
								if( !empty($image) ): 
									// vars
									$url = $image['url'];
									$title = $image['title'];
									// thumbnail
									$size = 'bones-thumb-100';
									$thumb = $image['sizes'][ $size ];
									$width = $image['sizes'][ $size . '-width' ];
									$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="photo" />
								<?php } else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" width="100px" height="100px" class="photo" />
								<?php } ?>
								<dl>
									<dt class="name"><?php the_title(); ?></dt>
									<dd class="position"><?php the_field('position_title'); ?></dd>
									<?php if(get_field('interest')) { ?>
									<dd class="interest">
										<!--<strong>Interest</strong>-->
										<?php the_field('interest'); ?>
									</dd>
									<?php } ?>
								</dl>
							</li>
						</a>

					<?php endwhile; ?>
					
					<?php while ( $emeriti_loop->have_posts() ) : $emeriti_loop->the_post(); ?>
					
						<a href="<?php the_permalink() ?>" class="person-item <?php the_field('language_of_study'); ?> <?php echo implode(' ', get_field('field')); ?>">
							<li><?php if(get_field('photo')) {
								$image = get_field('photo');
								if( !empty($image) ): 
									// vars
									$url = $image['url'];
									$title = $image['title'];
									// thumbnail
									$size = 'bones-thumb-100';
									$thumb = $image['sizes'][ $size ];
									$width = $image['sizes'][ $size . '-width' ];
									$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="photo" />
								<?php } else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" width="100px" height="100px" class="photo" />
								<?php } ?>
								<dl>
									<dt class="name"><?php the_title(); ?></dt>
									<dd class="position"><?php the_field('position_title'); ?></dd>
									<?php if(get_field('interest')) { ?>
									<dd class="interest">
										<!--<strong>Interest</strong>-->
										<?php the_field('interest'); ?>
									</dd>
									<?php } ?>
								</dl>
							</li>
						</a>

					<?php endwhile; ?>
					
					</ul>

				</div>

			</div>

<?php get_footer(); ?>
