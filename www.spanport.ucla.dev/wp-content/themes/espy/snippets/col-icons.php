<div class="col icon-col <?php the_sub_field('icon_width'); ?>">
	<h3><?php the_sub_field('icon_title'); ?></h3>
	<?php if( get_sub_field('add_image_widget') ) {
		$widget_link = get_sub_field('widget_link');
		$widget_title = get_sub_field('widget_title');
		$widget_text = get_sub_field('widget_text');
		$widget_background_image = get_sub_field('widget_background_image');
		
		$bottom_widget_link = get_sub_field('bottom_widget_link');
		$bottom_widget_title = get_sub_field('bottom_widget_title');
		$bottom_widget_text = get_sub_field('bottom_widget_text');
		$bottom_widget_background_image = get_sub_field('bottom_widget_background_image');
		
		if(!empty($widget_background_image)): 
			// vars
			$url = $widget_background_image['url'];
			$title = $widget_background_image['title'];
			// thumbnail
			$size = 'content-width';
			$widget_image = $widget_background_image['sizes'][ $size ];
			$width = $widget_background_image['sizes'][ $size . '-width' ];
			$height = $widget_background_image['sizes'][ $size . '-height' ];
		endif;
		
		if(!empty($bottom_widget_background_image)): 
			// vars
			$url = $bottom_widget_background_image['url'];
			$title = $bottom_widget_background_image['title'];
			// thumbnail
			$size = 'content-width';
			$bottom_widget_image = $bottom_widget_background_image['sizes'][ $size ];
			$width = $bottom_widget_background_image['sizes'][ $size . '-width' ];
			$height = $bottom_widget_background_image['sizes'][ $size . '-height' ];
		endif;
    	
    	if( get_sub_field('widget_location') == 'top' || get_sub_field('widget_location') == 'both' ) { ?>
			<?php if( $widget_link ): ?>
				<a href="<?php echo esc_url( $widget_link ); ?>" class="widget-link">
			<?php endif; ?>
				<div class="image-widget" style="background-image: url('<?php echo esc_url( $widget_image ); ?>');">
					<?php if($widget_title || $widget_text): ?>
						<div class="image-content">
					<?php endif; ?>
						<?php if($widget_title): ?>
							<h4><?php echo $widget_title; ?></h4>
						<?php endif; ?>
						<?php if($widget_text): ?>
							<span><?php echo $widget_text; ?></span>
						<?php endif; ?>
					<?php if($widget_title || $widget_text): ?>
						</div>
					<?php endif; ?>
				</div>
			<?php if( $widget_link ): ?>
				</a>
			<?php endif; ?>
		<?php }
	} ?>
	
	<?php if( have_rows('links') ): ?>
	
		<ul>
		<?php while( have_rows('links') ): the_row(); ?>
			<a href="<?php the_sub_field('link_url'); ?>">
				<li>
					<div class="icon">
						<?php the_sub_field('link_icon'); ?>
					</div>
					<div class="icon-text">
						<h4><?php the_sub_field('link_title'); ?></h4>
					</div>
				</li>
			</a>
		<?php endwhile; ?>
		</ul>
	
	<?php endif; ?>
	
	<?php if( get_sub_field('add_image_widget') ) {
		if( get_sub_field('widget_location') == 'bottom' || get_sub_field('widget_location') == 'both' ) { ?>
			<?php if( $widget_link ): ?>
				<a href="<?php echo esc_url( $bottom_widget_link ); ?>" class="widget-link">
			<?php endif; ?>
				<div class="image-widget" style="background-image: url('<?php echo esc_url( $bottom_widget_image ); ?>');">
					<?php if($bottom_widget_title || $bottom_widget_text): ?>
						<div class="image-content">
					<?php endif; ?>
						<?php if($bottom_widget_title): ?>
							<h4><?php echo $bottom_widget_title; ?></h4>
						<?php endif; ?>
						<?php if($bottom_widget_text): ?>
							<span><?php echo $bottom_widget_text; ?></span>
						<?php endif; ?>
					<?php if($bottom_widget_title || $bottom_widget_text): ?>
						</div>
					<?php endif; ?>
				</div>
			<?php if( $bottom_widget_link ): ?>
				</a>
			<?php endif; ?>
		<?php } 
	} ?>
</div>