<?php // For Event month or list landing page, https://gist.github.com/jo-snips/2415009
// Only run if The Events Calendar is installed 
if ( tribe_is_past() || tribe_is_upcoming() && !is_tax() || tribe_is_month() && !is_tax() || tribe_is_upcoming() && is_tax()) { 
	// Do nothing	
}
// For posts
elseif (is_single() || is_category() || is_search()) { ?>
<div class="col side feed" role="complementary">
	<?php if ( is_active_sidebar( 'news-sidebar' ) ) :  ?>
	<?php dynamic_sidebar( 'news-sidebar' ); ?>
	<?php else : endif; ?>
	<?php if ( is_active_sidebar( 'events-sidebar' ) ) :  ?>
	<?php dynamic_sidebar( 'events-sidebar' ); ?>
	<?php else : endif; ?>
</div>
<?php } ?>
<?php // For pages
if (is_page() || is_404()) { ?>
<div class="col side">
	<div class="content">
		<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
			<?php
				// If a Graduate subpage or Group page
				if (is_tree(135) or get_field('menu_select') == "graduate" or get_post_type() == "groups_type") {
					wp_nav_menu(array(
						'container' => false,
						'menu' => __( 'Graduate', 'bonestheme' ),
						'menu_class' => 'grad-nav',
						'theme_location' => 'grad-nav',
						'before' => '',
						'after' => '',
						'depth' => 2,
						'items_wrap' => '<h3>Graduate</h3> <ul>%3$s</ul>'
					));
				}
				
				// If an Undergraduate subpage
				if (is_tree(133) or get_field('menu_select') == "undergraduate") {
					wp_nav_menu(array(
					   	'container' => false,
					   	'menu' => __( 'Undergraduate', 'bonestheme' ),
					   	'menu_class' => 'undergrad-nav',
					   	'theme_location' => 'undergrad-nav',
					   	'before' => '',
					   	'after' => '',
					   	'depth' => 2,
					   	'items_wrap' => '<h3>Undergraduate</h3> <ul>%3$s</ul>'
					));
				}	
					
				// For Research section
				if (get_field('menu_select') == "research") {
					wp_nav_menu(array(
						'container' => false,
						'menu' => __( 'Research', 'bonestheme' ),
						'menu_class' => 'research-nav',
						'theme_location' => 'research-nav',
						'before' => '',
						'after' => '',
						'depth' => 2,
						'items_wrap' => '<h3>Research Topics</h3> <ul>%3$s</ul>'
					));
				}
				
				// For Resources section
				if (get_field('menu_select') == "resources") {
					wp_nav_menu(array(
						'container' => false,
						'menu' => __( 'Resources', 'bonestheme' ),
						'menu_class' => 'resources-nav',
						'theme_location' => 'resources-nav',
						'before' => '',
						'after' => '',
						'depth' => 2,
						'items_wrap' => '<h3>Resources</h3> <ul>%3$s</ul>'
					));
				}
				
				// For Research 
				if (get_post_type() == "research_type") {
					wp_nav_menu(array(
						'container' => false,
						'menu' => __( 'Research', 'bonestheme' ),
						'menu_class' => 'research-nav',
						'theme_location' => 'research-nav',
						'before' => '',
						'after' => '',
						'depth' => 2,
						'items_wrap' => '<h3>Research Topics</h3> <ul class="research-nav">%3$s</ul>'
					));
				}
				
				// For People 
				if (is_page_template('page-memoriam.php')) {
					wp_nav_menu(array(
						'container' => false,
						'menu' => __( 'People', 'bonestheme' ),
						'menu_class' => 'people-nav',
						'theme_location' => 'people-nav',
						'before' => '',
						'after' => '',
						'depth' => 2,
						'items_wrap' => '<h3>People</h3> <ul class="people-nav">%3$s</ul>'
					));
				}
				
				// For general pages, Resources, Alumni, Search, and 404's
				if (is_tree(854) or is_tree(941) or is_search() or is_404() or get_field('menu_select') == "general") {
					wp_nav_menu(array(
						'container' => false,
						'menu' => __( 'Main Menu', 'bonestheme' ),
						'menu_class' => 'side-nav',
						'theme_location' => 'main-nav',
						'before' => '',
						'after' => '',
						'depth' => 2,
						'items_wrap' => '<h3>Main Menu</h3> <ul>%3$s</ul>'
					));
				}
			?>
		</nav>
	</div>
</div>
<?php } ?>