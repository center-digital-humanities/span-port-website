<?php get_header(); ?>
			<div class="content main">
			
				<div class="col">
					<h1 class="archive-title">Graduate Student Organizations</h1>

					<?php $groups_loop = new WP_Query( array( 'post_type' => 'groups_type', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'asc' ) ); ?>
					
					<?php while ( $groups_loop->have_posts() ) : $groups_loop->the_post(); ?>

					<h2 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
					<section>
						<p><?php
						$content = get_the_content();
						$trimmed_content = wp_trim_words( $content, 50, '...' );
						echo $trimmed_content;
						?></p>
					</section>
					<?php endwhile; ?>						
				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>