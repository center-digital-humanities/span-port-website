<?php get_header(); ?>

			<div class="content main">
			
				<header>
					<h1><?php single_cat_title(); ?></h1>
					<!--<p>Our faculty consist of over 20 of the most talented minds in the field.</p>-->
					<!--<input type="search" class="search-filter" placeholder="Type a name..." />-->
					<div class="filter">
						<div class="fields button-group" data-filter-group="field">
							<h4>Field of Study</h4>
							<ul>
								<button class="button btn all is-checked" data-filter="">All</button>
								<button class="button btn literature" data-filter=".literature">Literature/Culture</button>
								<button class="button btn" data-filter=".linguistics">Linguistics</button>
							</ul>
						</div>
						<div class="language button-group" data-filter-group="language">
							<h4>Language</h4>
							<ul>
								<button class="button btn all is-checked" data-filter="">All</button>
								<button class="button btn" data-filter=".Spanish">Spanish</button>
								<button class="button btn" data-filter=".Portuguese">Portuguese</button>
							</ul>
						</div>
					</div>		
				</header>

				<div class="people-list">

					<ul <?php post_class('cf'); ?>>
					
					<?php 
						$people_loop = new WP_Query( array( 'post_type' => 'people_type', 'people_cat' => 'grad', 'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC', 'posts_per_page' => -1 )); 
					?>
					
					<?php while ( $people_loop->have_posts() ) : $people_loop->the_post(); ?>
										
						<a href="<?php the_permalink() ?>" class="person-item <?php the_field('language_of_study'); ?> <?php the_field('field'); ?>">
							<li><?php if(get_field('photo')) {
								$image = get_field('photo');
								if( !empty($image) ): 
									// vars
									$url = $image['url'];
									$title = $image['title'];
									// thumbnail
									$size = 'bones-thumb-100';
									$thumb = $image['sizes'][ $size ];
									$width = $image['sizes'][ $size . '-width' ];
									$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="photo" />
								<?php } else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" width="100px" height="100px" class="photo" />
								<?php } ?>
								<dl>
									<dt class="name"><?php the_title(); ?></dt>
									<dd class="position"><?php the_field('position_title'); ?></dd>
									<?php if(get_field('interest')) { ?>
									<dd class="interest">
										<!--<strong>Interest</strong>-->
										<?php the_field('interest'); ?>
									</dd>
									<?php } ?>
								</dl>
							</li>
						</a>
	
					<?php endwhile; ?>
					</ul>

				</div>

			</div>

<?php get_footer(); ?>