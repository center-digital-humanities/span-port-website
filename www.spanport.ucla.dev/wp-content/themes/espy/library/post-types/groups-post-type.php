<?php
// Courses Post Type Settings

// let's create the function for the custom type
function groups_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'groups_type', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Groups', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Group', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Groups', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Group', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Group', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Group', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Group', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Groups', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No groups added yet.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Contains all groups', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 9, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-groups', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'group', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'groups', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => true,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt')
		) /* end of options */
	); /* end of register post type */	
}

// adding the function to the Wordpress init
add_action( 'init', 'groups_post_type'); ?>