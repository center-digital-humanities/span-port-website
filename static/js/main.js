// Code for fixed scrolling menus
jQuery("document").ready(function($){
    
    var nav = $('.col-nav');
    
    $(window).scroll(function () {
        if ($(this).scrollTop() > 550) {
            nav.addClass("fixed");
        } else {
            nav.removeClass("fixed");
        }
    });
 
});

//Smooth scrolling effect when using anchor links
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});